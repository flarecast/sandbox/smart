S.M.A.R.T Algorithm - Python Implementation
===========================================

1. Introduction
---------------
This repository contains an implementation of the [S.M.A.R.T Algorithm](http://arxiv.org/abs/1006.5898) in python.
In addition there is an implementation for feature tracking, which is not really described in the paper.

2. Requirements
---------------
The flarecast pipeline must be running, providing access to hmi images and a database for writing properties.
For further information read https://dev.flarecast.eu/stash/projects/INFRA/repos/dev-infra/browse





